﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventCreater
{
    /// <summary>
    /// 出現条件
    /// </summary>
    public partial struct AppearCondition
    {
        // 出現条件フラグ
        public bool checkFlag;
        public string eventName;
        public int flagNo;

        // 出現条件変数
        public bool checkVariable;
        public int valNo;
        public int valLimit;
        public enum Condition { Eq, More, OneOrLess, Greater, Less, Other }
        public Condition condition;

        // 出現条件メンバー
        public bool checkMen;
        public int menNo;

        /// <summary>
        /// 疑似TKCode。
        /// ここからファイルに保存。
        /// </summary>
        public static List<string> codes = new List<string>();

        public AppearCondition(bool cff, int fn, bool cvf, int vln, int vlm, Condition cond, bool mnf, int mno, string ename = "名無しのイベント")
        {
            eventName = ename;
            checkFlag = cff;
            flagNo = fn;
            checkVariable = cvf;
            valNo = vln;
            valLimit = vlm;
            condition = cond;
            checkMen = mnf;
            menNo = mno;
        }

        public void FlagSet(int fl, int n)
        {
            flagNo = n;
            checkFlag = Convert.ToBoolean(fl);
        }

        public void ValSet(int fl, int n, int lim, int cond)
        {
            valNo = n;
            valLimit = lim;
            condition = (Condition)cond;
            checkVariable = Convert.ToBoolean(fl);
        }

        public void MenmSet(int fl, int n)
        {
            menNo = n;
            checkMen = Convert.ToBoolean(fl);
        }
    }

    /// <summary>
    /// イベントのコンテンツ
    /// </summary>
    public partial struct Contents
    {
        public Contents(List<string> a = null)
        {
            strs = new List<string>();
            codes = new List<string>();
            strs.Add("\t");
            codes.Add("\t");
        }

        public readonly static string[] temp =
        {
            "Appearance condition",
            "{",
            "\tflag(0,0)",
            "\tval(0,0,0,0)",
            "\tmenm(1,2)",
            "\t",
            "}",
            "Event content",
            "{",
            "\t",
            "}"
        };

        /// <summary>
        /// リストボックスに追加するアイテム。
        /// 説明文章になる。
        /// </summary>
        public List<string> strs;

        /// <summary>
        /// 疑似TKCode。
        /// ここから変換してファイルとstrsにそれぞれ保存。
        /// </summary>
        public List<string> codes;

        /// <summary>
        /// コードを説明文に変換。
        /// </summary>
        public void CodeConversionToStrs()
        {
            //TODO:編集中
            strs.Clear();
            strs.AddRange(codes.ToArray());
        }
        
        /// <summary>
        /// イベント編集画面のリストボックスにstrsを保存。
        /// </summary>
        public void SaveEventStrsToForm()
        {
            EventForm.eventForm.EVConts.Items.Clear();
            EventForm.eventForm.EVConts.Items.AddRange(strs.ToArray());
        }
    }

    public static class EventData
    {
        /// <summary>
        /// 初期化
        /// </summary>
        public static void ResetEventData()
        {
            AppStrs = new List<string>();
            EvcStrs = new List<string>();
            apCond = new AppearCondition();
            EventID = 0;
            apCond = new AppearCondition(false, 0, false, 0, 0, AppearCondition.Condition.Eq, false, 0);
            evCont = new Contents(null);
        }

        //--------------EventForm-------------------
        //--------------EventContentsForm-------------------

        /// <summary>
        /// 出現条件(テキスト)
        /// </summary>
        public static List<string> AppStrs;
        /// <summary>
        /// イベントコンテンツ(テキスト)
        /// </summary>
        public static List<string> EvcStrs;
        /// <summary>
        /// 出現条件(データ)
        /// </summary>
        public static AppearCondition apCond;
        /// <summary>
        /// イベントコンテンツ(データ)
        /// </summary>
        public static Contents evCont;

        /// <summary>
        /// 現在のイベントID。
        /// EV + this.ToString("D4")でラベルにもなる。
        /// </summary>
        public static int EventID = 0;

        /// <summary>
        /// ラベル形式でidを返す。
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private static string GetLabelName(int id)
        {
            return "EV" + id.ToString("D4");
        }

        /// <summary>
        /// イベントコードを登録する。
        /// </summary>
        public static void SetCodeFromEventActList(string[] code, int isedit)
        {
            EventForm.eventForm.EventContentsSet(code, isedit);
        }

        /// <summary>
        /// codeをstrsに変換、代入
        /// </summary>
        public static void CodeToStrs()
        {
            evCont.strs.Clear();

            evCont.strs.AddRange(evCont.codes.ToArray());
            // \tを省く
            RemoveTabAndLine(evCont.strs);
            // インデント用の全角スペースを追加する。
            AddTab(evCont.strs);
        }
        
        /// <summary>
        /// タブ文字の消去
        /// </summary>
        /// <param name="strs"></param>
        private static void RemoveTabAndLine(List<string> strs)
        {
            for (int i = 0; i < evCont.strs.Count; i++)
            {
                strs[i] = CollectionTool.RemoveChar(strs[i], '\t');
                strs[i] = CollectionTool.RemoveChar(strs[i], '\n');
            }
        }

        /// <summary>
        /// 条件分岐で全角スペースの追加
        /// </summary>
        /// <param name="strs"></param>
        private static void AddTab(List<string> strs)
        {
            int stck = 0;
            int fcn = 0;
            for(int i = 0; i < strs.Count; i++)
            {
                int c = CheckWithTabString(strs[i]);
                stck = (c == 1 || c == 2 || c == 3 || c == 4) ? stck - 1 : stck;
                if (fcn > 0)
                {
                    fcn--;
                    stck++;
                }

                for (int h = 0; h < stck; h++)
                {
                    strs[i] = strs[i] + "　";
                }

                c = CheckWithTabString(strs[i]);
                if (c != -1)
                {
                    switch (c)
                    {
                        case 0:
                            stck++;
                            break;
                        case 1:
                            stck++;
                            break;
                        case 3:
                            stck++;
                            break;
                        case 5:
                            fcn++;
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// 全角スペースを追加する文字列をチェック
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static int CheckWithTabString(string str)
        {
            string[] words = { "If", "Else", "Endif", "Branch", "EndCho", "Choice" };
            for (int i = 0; i < words.Length; i++)
            {
                string s = (str.Length > 6) ? str.Substring(0, 6) : str;
                if (s.IndexOf(words[i]) != -1)
                    return i;
            }
            return -1;
        }


        /// <summary>
        /// strsをフォームに代入
        /// </summary>
        public static void StrsToForm()
        {
            EventForm.eventForm.EVConts.Items.Clear();
            for (int i = 0; i < evCont.strs.Count; i++)
            {
                //if (evCont.strs[i] != "")
                //{
                    // ◆追加
                    string symbol = GetFirstSymbol(evCont.strs[i]);
                    // 全角スペース追加
                    symbol = GetSpace(symbol, evCont.strs[i]);
                    // 表示用文字列の取得
                    string viewString = GetViewString(evCont.strs[i]);
                    EventForm.eventForm.EVConts.Items.Add(symbol + viewString);
                //}
            }
        }

        /// <summary>
        /// 表示用文字列の取得
        /// </summary>
        /// <param name="str"></param>
        private static string GetViewString(string str)
        {
            string oldstr = InstructionAnalysis.SeachWordAsString(str);
            string s_inst = InstructionAnalysis.GetViewStrs(str);
            if (oldstr == null || s_inst == null) return str;
            // MicrosoftVisualBasic.Strings.Replace特有の置き換え回数の設定を使う
            str = Microsoft.VisualBasic.Strings.Replace(str, oldstr, s_inst, 1, 1);
            return str;
        }

        /// <summary>
        /// シンボル文字列に全角スペースの数だけ全角スペースを前に追加して返す。
        /// </summary>
        /// <param name="symbol"></param>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetSpace(string symbol, string str)
        {
            int cnt = str.Length - 1;
            if (cnt < 0) return symbol;

            string st = str.Substring(cnt, 1);
            while (st == "　")
            {
                symbol = "　" + symbol;
                cnt--;
                if (-1 == cnt)
                    return symbol;
                st = str.Substring(cnt, 1);
            }
            return symbol;
        }


        /// <summary>
        /// リストボックスに表示する文章の接頭記号を返す。
        /// ◆を含むコンテンツ以外の右クリックの場合、直接編集画面に飛べない。
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetFirstSymbol(string str)
        {
            string[] Words = { "SubT(", "SubN(", "Else:", "Endif:",
                                "EndChoice:", "Branch("};
            string[] N_Words = { "Note(", "NoWriteLine:" };
            foreach(var a in Words)
            {
                if (str.IndexOf(a) != -1)
                    return "＿";
            }
            foreach(var a in N_Words)
            {
                if (str.IndexOf(a) != -1)
                    return "★";
            }
            return "◆";
        }

        /// <summary>
        /// イベントデータの保存
        /// </summary>
        public static void SaveEventData()
        {
            List<string> dat = new List<string>();
            dat = DirectoryUtils.ReadFile("Events", "List");
            int numEv = int.Parse(dat[0].Split(':')[1]);
            SaveMetaData(dat, numEv);
            SaveConditionData(dat, numEv);
            SaveContentsData(dat, numEv);

            DirectoryUtils.SaveFile("Events", "List", dat);
        }

        private static void SaveMetaData(List<string> dat, int numEv)
        {
            // メタデータの中にあるラベルのindexを取得。
            int idx = CollectionTool.GetIndexLabel(dat, GetLabelName(EventID) + "}", 1, numEv + 1);
            // メタデータのイベント名+{ラベル}をListに保存
            if (idx != -1)
            {
                // イベントタイトルの変更
                string str = dat[idx];
                string[] ss = str.Split('{');
                ss[0] = apCond.eventName;
                dat[idx] = ss[0] + "{" + ss[1];
            }
            else
            {
                // イベントメタデータの新規作成
                dat.Insert(numEv + 1, apCond.eventName + "{" + GetLabelName(EventID) + "}");
                // イベントコンテンツのテンプレートを挿入する。
                dat.Insert(dat.Count, GetLabelName(EventID) + ":" + apCond.eventName);
                dat.InsertRange(dat.Count, Contents.temp);
                dat[0] = "NumEvent:" + (numEv + 1).ToString();
            }
        }

        private static void SaveConditionData(List<string> dat, int numEv)
        {
            // メタデータの中にあるラベルのindexを取得。
            int idx = CollectionTool.GetIndexLabel(dat, GetLabelName(EventID) + ":", numEv + 1);
            int endidx = CollectionTool.GetIndexLabel(dat, "Event content", idx + 1);
            // メタデータのイベント名+{ラベル}をListに保存
            if (idx != -1 && endidx != -1)
            {
                dat[idx] = GetLabelName(EventID) + ":" + apCond.eventName;
                // テキストの入れ替え。
                idx += 3; endidx -= 2;
                dat.RemoveRange(idx, endidx - idx + 1);
                string[] apcode = GetApCode();
                dat.InsertRange(idx, apcode);
            }
            else
            {
                MessageBox.Show("List.datにコンディションテキストがありません。",
                                "エラー",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private static void SaveContentsData(List<string> dat, int numEv)
        {
            // メタデータの中にあるラベルのindexを取得。
            int idx = CollectionTool.GetIndexLabel(dat, GetLabelName(EventID) + ":", numEv + 1);
            int binidx = CollectionTool.GetIndexLabel(dat, "Event content", idx + 1);
            int endidx = CollectionTool.GetIndexLabel(dat, "}", binidx + 1);
            // メタデータのイベント名+{ラベル}をListに保存
            if (idx != -1 && endidx != -1 && binidx != -1)
            {
                // テキストの入れ替え。
                binidx += 2; endidx -= 1;
                dat.RemoveRange(binidx, endidx - binidx + 1);
                string[] eccode = GetEcCode();
                dat.InsertRange(binidx, eccode);
            }
            else
            {
                MessageBox.Show("List.datに コンテンツテキストがありません。",
                                "エラー",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private static string[] GetApCode()
        {
            string[] strs = new string[3];
            int[] args = new int[8];
            args[0] = Convert.ToInt32(apCond.checkFlag);
            args[1] = apCond.flagNo;
            args[2] = Convert.ToInt32(apCond.checkVariable);
            args[3] = apCond.valNo;
            args[4] = apCond.valLimit;
            args[5] = (int)apCond.condition;
            args[6] = Convert.ToInt32(apCond.checkMen);
            args[7] = apCond.menNo;

            strs[0] = "\tflag" + "(" + args[0] + "," + args[1] + ")";
            strs[1] = "\tval" + "(" + args[2] + "," + args[3] + "," + args[4] + "," + args[5] + ")";
            strs[2] = "\tmenm" + "(" + args[6] + "," + args[7] + ")";

            return strs;
        }

        private static string[] GetEcCode()
        {
            return evCont.codes.ToArray();
        }

        //--------------MainForm-------------------

        /// <summary>
        /// ファイルから読み込んだイベント数
        /// </summary>
        public static int NumEvent = 0;
        /// <summary>
        /// 現在の選択しているイベントID
        /// </summary>
        public static int NowEventNumber = -1;
        /// <summary>
        /// 解析するデータバッファ
        /// </summary>
        public static List<string> srcBuf = new List<string>();
        /// <summary>
        /// 初回時に書き込む
        /// </summary>
        private static string DefaultEventList = "NumEvent:0";

        /// <summary>
        /// イベントの表示する文字を準備
        /// </summary>
        public static void BufToEventNameAndLabels(ref string[] viewbf, ref string[] lbls)
        {
            viewbf = new string[EventData.NumEvent];
            lbls = new string[EventData.NumEvent];
            for (int i = 0; i < EventData.NumEvent; i++)
            {
                var ars = EventData.srcBuf[i + 1].Split('{');
                string Name = ars[0];
                string label = ars[1].Substring(0, ars[1].Length - 1);

                viewbf[i] = Name;
                lbls[i] = label;
            }
        }

        /// <summary>
        /// イベントリストの読み込み
        /// </summary>
        public static void LoadEventListFormFile()
        {
            EventData.srcBuf = new List<string>();
            EventData.srcBuf = DirectoryUtils.ReadFile("Events", "List");
            // 初回開いた時
            if (EventData.srcBuf.Count == 0)
            {
                EventData.srcBuf.Add(EventData.DefaultEventList);
                DirectoryUtils.SaveFile("Events", "List", EventData.srcBuf);
            }
        }

    }
}
