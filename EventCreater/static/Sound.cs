﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventCreater
{
    public static class Sound
    {

        /// <summary>
        /// ファイル名
        /// </summary>
        public static string[] fname = { "bgm/listb", "se/lists" };

        /// <summary>
        /// サウンドのタイトル
        /// </summary>
        public static List<string> soundName;
        /// <summary>
        /// サウンドの用途
        /// </summary>
        public static List<string> soundUse;

        /// <summary>
        /// サウンドの長さ
        /// </summary>
        public static List<string> soundLength;

        /// <summary>
        /// サウンドの拡張子
        /// </summary>
        public static List<string> soundExtension;

        /// <summary>
        /// サウンドのメモ
        /// </summary>
        public static List<string> soundNotes;

        /// <summary>
        /// サウンドのパス
        /// </summary>
        public static List<string> soundPath;

        /// <summary>
        /// サウンドの初期最大数(ファイルより読み込み)
        /// </summary>
        private const int InitNumSound = 20;

        /// <summary>
        /// <summary>
        /// サウンドの最大数(ファイルより読み込み)
        /// </summary>
        public static int NumSound;

        /// <summary>
        /// 初期フォーマットテキスト
        /// </summary>
        private static string[] initStrTemp =
            {"タイトル,用途,長さ,拡張子,メモ,ファイル名,,"};

        /// <summary>
        /// ファイルを読み込む。サウンド、フラグファイルの名前を指定する。
        /// </summary>
        /// <param name="fileName"></param>
        public static string[] ReadSoundFile(string filepath)
        {

            if (!File.Exists(AppPath.path + "/" + filepath + ".csv"))
            {
                // CreateSoundFile(fileName);
                MessageBox.Show(AppPath.path + "/" + filepath + ".csvがありません。",
                                "エラー",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return null;
            }
            FileStream fs = new FileStream(AppPath.path + "/" + filepath + ".csv", FileMode.Open, FileAccess.Read);
            try
            {
                //抜けると同時に削除
                using (StreamReader sr = new StreamReader(fs))
                {
                    sr.ReadLine();

                    soundName = new List<string>();
                    soundUse = new List<string>();
                    soundLength = new List<string>();
                    soundExtension = new List<string>();
                    soundNotes = new List<string>();
                    soundPath = new List<string>();

                    int i = 0;
                    //一行ごとに読み込める
                    while (!sr.EndOfStream)
                    {
                        // ファイルから一行読み込む
                        var line = sr.ReadLine();

                        // データを分ける
                        var ar = line.Split(',');

                        if (ar[0] != "metadata" && ar[5] != "")
                        {
                            soundName.Add(ar[0]);
                            soundUse.Add(ar[1]);
                            soundLength.Add(ar[2]);
                            soundExtension.Add((ar[3] == "") ? "" : "." + ar[3]);
                            soundNotes.Add(ar[4]);
                            soundPath.Add(ar[5] + soundExtension[soundExtension.Count - 1]);

                            i++;
                        }
                    }
                    NumSound = i;
                }
            }
            catch
            {
                MessageBox.Show("サウンド、フラグファイルのフォーマットが壊れているかもしれません。\n直してから開いてみてください。",
                                "エラー",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            fs.Close();
            return soundName.ToArray();
        }
    }
}
