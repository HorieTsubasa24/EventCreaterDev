﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class VariableForm : Form
    {
        /// <summary>
        /// 一つのページに含まれる変数の数。
        /// </summary>
        private const int NumOfPage = 20;

        /// <summary>
        /// 変数範囲のページ
        /// -1で非選択
        /// </summary>
        private int valPage = 0;

        /// <summary>
        /// 選択している変数
        /// -1で非選択
        /// </summary>
        private int valIndex = 0;

        /// <summary>
        /// フラグモードと変数モードに分ける。
        /// </summary>
        private int ModeVariable;

        /// <summary>
        /// デリゲードメソッドを入れることで変数フラグを選択できる。
        /// </summary>
        /// <param name="mode">0:flag 1:val 2:status 5:chara</param>
        public VariableForm(int mode, DoRetValNum del = null)
        {
            InitializeComponent();

            retNum = del;

            
            ModeVariable = mode;

            if (mode == 0)
            {
                labelVariavle.Text = "フラグ";
                this.Text = "フラグリスト";
            }
            else if (mode == 1)
            {
                labelVariavle.Text = "変数";
                this.Text = "変数リスト";
            }
            else if (mode == 2)
            {
                labelVariavle.Text = "状態異常";
                this.Text = "状態異常リスト";
            }
            else if (mode == 5)
            {
                labelVariavle.Text = "キャラクター";
                this.Text = "キャラクターリスト";
            }

            Variable.ReadVariableFile(Variable.fname[mode]);
            ShowVariablePageList();
            ShowVariableList();
            valPage = listBoxVariableBlocks.SelectedIndex = (listBoxVariableBlocks.Items.Count != 0) ? 0 : -1;
            valIndex = listBoxVariables.SelectedIndex = (listBoxVariables.Items.Count != 0) ? 0 : -1;
        }

        public delegate void DoRetValNum(int num, string vname);
        /// <summary>
        /// 値を返すためのコールバック用デリゲード
        /// </summary>
        private DoRetValNum retNum;

        /// <summary>
        /// 変数群の表示
        /// </summary>
        private void ShowVariables()
        {
            ShowVariablePageList();
            ShowVariableList();
            listBoxVariableBlocks.SelectedIndex = valPage;
            listBoxVariables.SelectedIndex = valIndex;
        }

        /// <summary>
        /// 有効化時にフォーム内容セット
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void VariableForm_EnabledChanged(object sender, EventArgs e)
        {
            if (this.Enabled == true)
            {
                ShowVariables();
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 最大数の変更フォームを開く
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonMaxVariablesChange_Click(object sender, EventArgs e)
        {
            // 範囲外の時、範囲内に値をセット
            if (listBoxVariableBlocks.SelectedIndex == -1 ||
                listBoxVariables.SelectedIndex == -1)
            {
                valPage = listBoxVariableBlocks.SelectedIndex = 0;
                valIndex = listBoxVariables.SelectedIndex = 0;
            }
            VariableMaxChangeForm vmf = new VariableMaxChangeForm(Variable.fname[ModeVariable]);
            CloseAct.FormOpenCommon(this, vmf);
        }

        /// <summary>
        /// フォームを閉じる。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 変数範囲クリック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBoxVariableBlocks_Click(object sender, EventArgs e)
        {
            // SelectedItem = 文字値
            // SelectedIndex = 0 ~ n
            ListBox lb = (ListBox)sender;
            valPage = lb.SelectedIndex;
            ShowVariableList(valPage);
        }

        /// <summary>
        /// 変数クリック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBoxVariables_Click(object sender, EventArgs e)
        {
            // SelectedItem = 文字値
            // SelectedIndex = 0 ~ n
            ListBox lb = (ListBox)sender;
            valIndex = lb.SelectedIndex;

            string valName = "NoneName";
            if (valIndex != -1 && valPage != -1)
                valName = Variable.variableName[valPage * NumOfPage + valIndex];

            labelVariableNumber.Text = (valPage * NumOfPage + valIndex + 1).ToString("D4") + ":";
            textBoxVariableName.Text = valName;
        }

        /// <summary>
        /// 変数ページリスト表示
        /// </summary>
        private void ShowVariablePageList()
        {
            List<string> st = new List<string>();
            st = Variable.SetVariablePageString(NumOfPage);
            listBoxVariableBlocks.Items.Clear();
            for (int i = 0; i < st.Count; i++)
                listBoxVariableBlocks.Items.Add(st[i]);
        }

        /// <summary>
        /// 変数リスト表示
        /// </summary>
        private void ShowVariableList(int page = 0)
        {
            if (page == -1) return;
            List<string> st = new List<string>();
            st = Variable.SetVariableString(st);
            listBoxVariables.Items.Clear();
            for (int n = 0, i = page * NumOfPage ; (n < NumOfPage) && (n + valPage * NumOfPage < Variable.NumVariable); i++, n++)
                listBoxVariables.Items.Add(st[valPage * NumOfPage + n]);
        }
        
        /// <summary>
        /// テキスト入力時、エンターキーが押されると変数名保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxVariableName_KeyDown(object sender, KeyEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (e.KeyCode == Keys.Enter)
            {
                VariableNameSet(tb);
            }
        }
       
        /// <summary>
        /// テキスト入力時、テキストボックスを離れると保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxVariableName_Leave(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            VariableNameSet(tb);
        }

        /// <summary>
        /// 変数名セット
        /// </summary>
        /// <param name="tb"></param>
        private void VariableNameSet(TextBox tb)
        {
            if (tb.Text == "")
                Variable.variableName[valPage * NumOfPage + valIndex] = " ";
            else
                Variable.variableName[valPage * NumOfPage + valIndex] = tb.Text;

            Variable.SaveVariableFile(Variable.fname[ModeVariable]);
            ShowVariables();
            Console.WriteLine(valPage * NumOfPage + valIndex + ":" + tb.Text);
        }

        /// <summary>
        /// フォーカスが移った時、" "だと文字なしにしてから入力させる。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxVariableName_Enter(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text == " ")
                tb.Text = "";
        }
        
        /// <summary>
        /// OKボタンで閉じる
        /// 変数、フラグ選択時のときは選択変数、フラグを拾う。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOK_Click(object sender, EventArgs e)
        {
            // 閉じるときにデリゲードメソッドを実行する。
            if (retNum != null)
            {
                if (listBoxVariableBlocks.SelectedIndex == -1)
                    listBoxVariableBlocks.SelectedIndex = 0;

                int index = listBoxVariables.SelectedIndex;
                int page = listBoxVariableBlocks.SelectedIndex;
                int Number = index + page * NumOfPage;
                if (!(index < 0 || Number >= Variable.NumVariable))
                    retNum.Invoke(Number, (1 + Number).ToString("D4") + ":" + Variable.variableName[Number]);
                retNum = null;
            }
            this.Close();
        }
    }
}
