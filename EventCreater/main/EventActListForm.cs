﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class EventActListForm : Form
    {
        /// <summary>
        /// 外部から参照するときに使う。
        /// </summary>
        public static Form eventActListForm;

        public EventActListForm()
        {
            InitializeComponent();
            eventActListForm = this;
        }

        /// <summary>
        /// フォームを閉じるときに参照用の参照を切る。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EventActListForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            eventActListForm = null;
        }

        /// <summary>
        /// 文章の表示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonText_Click(object sender, EventArgs e)
        {
            Form fm = new TextForm(this);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// 経験値の増減。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonExp_Click(object sender, EventArgs e)
        {
            Form fm = new ExpForm(this);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// 選択肢の表示。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonChoice_Click(object sender, EventArgs e)
        {
            Form fm = new ChoiceForm(this);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// フラグの操作。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonFlag_Click(object sender, EventArgs e)
        {
            Form fm = new FlagForm(this);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// 変数の操作。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonVariable_Click(object sender, EventArgs e)
        {
            Form fm = new VForm(this);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// お金の増減。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonMoney_Click(object sender, EventArgs e)
        {
            Form fm = new MoneyForm(this);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// アイテムの操作。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonItem_Click(object sender, EventArgs e)
        {
            Form fm = new IForm(this);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// キャラクターの表示。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonChara_Click(object sender, EventArgs e)
        {
            Form fm = new CharaForm(this);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// キャラクターの消去。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCharadel_Click(object sender, EventArgs e)
        {
            Form fm = new CharaDelForm(this);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// スキルの操作。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSkill_Click(object sender, EventArgs e)
        {
            Form fm = new SkillForm(this);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// HpMpの増減。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonHpMp_Click(object sender, EventArgs e)
        {
            Form fm = new HPMPForm(this);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// 全回復。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCareAll_Click(object sender, EventArgs e)
        {
            Form fm = new CareForm(this);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// 画面の色調変更。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonColorTone_Click(object sender, EventArgs e)
        {
            Form fm = new WindowColorForm(this);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// 画面のフラッシュ。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonFlash_Click(object sender, EventArgs e)
        {
            Form fm = new FlashForm(this);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// 画面を揺らす。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonShake_Click(object sender, EventArgs e)
        {
            Form fm = new ShakeForm(this);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// エフェクトの再生。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonEffect_Click(object sender, EventArgs e)
        {
            Form fm = new EffectForm(this);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// メンバーの増減。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonMen_Click(object sender, EventArgs e)
        {
            Form fm = new MemberFormcs(this);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// レベルの増減。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonLv_Click(object sender, EventArgs e)
        {
            Form fm = new LevelForm(this);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// 注釈、メモ。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonNote_Click(object sender, EventArgs e)
        {
            Form fm = new NoteForm(this);
            CloseAct.FormOpenCommon(this, fm);
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 条件分岐
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBranch_Click(object sender, EventArgs e)
        {
            Form fm = new BranchForm(this);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// BGMの再生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBGM_Click(object sender, EventArgs e)
        {
            Form fm = new SoundForm(0, this, 0);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// SEの再生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSE_Click(object sender, EventArgs e)
        {
            Form fm = new SoundForm(1, this, 0);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// BGMのフェードアウト
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBGMStop_Click(object sender, EventArgs e)
        {
            Form fm = new BGMFadeOutForm(this);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// キャラ名表示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCharaname_Click(object sender, EventArgs e)
        {
            Form fm = new CharaName(this);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// 空白行挿入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NoWriteLine_Click(object sender, EventArgs e)
        {
            int isEdit = 0;
            string[] code = { "NoWriteLine:" };
            EventData.SetCodeFromEventActList(code, isEdit);
            this.Close();
        }

        /// <summary>
        /// お店の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Shop_Click(object sender, EventArgs e)
        {
            Form fm = new ShopForm(this);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// 状態の変更
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StatusChange_Click(object sender, EventArgs e)
        {
            Form fm = new StatusChangeForm(this);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// 場所移動
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlaceMove_Click(object sender, EventArgs e)
        {
            Form fm = new PlaceMoveForm(this);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// 数値入力
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NumInput_Click(object sender, EventArgs e)
        {
            Form fm = new NumInputForm(this);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// 他イベントの呼び出し
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CallEvent_Click(object sender, EventArgs e)
        {
            Form fm = new EventCallForm(this);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// 戦闘の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Battle_Click(object sender, EventArgs e)
        {
            Form fm = new BattleForm(this);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// 拠点の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Camp_Click(object sender, EventArgs e)
        {
            int isEdit = 0;
            string[] code = { "Camp:" };
            EventData.SetCodeFromEventActList(code, isEdit);
            this.Close();
        }

        /// <summary>
        /// ラベルの設置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SetLabel_Click(object sender, EventArgs e)
        {
            Form fm = new LabelSetForm(this);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// 指定のラベルに飛ぶ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GotoLabel_Click(object sender, EventArgs e)
        {
            Form fm = new LabelGotoForm(this);
            CloseAct.FormOpenCommon(this, fm);
        }
    }
}
