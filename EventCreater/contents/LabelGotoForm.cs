﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class LabelGotoForm : Form
    {
        EventContentsForm ecf;
        public LabelGotoForm(Form parent, int mode = 0, int[] e_Args = null)
        {
            InitializeComponent();

            ArgsSet(e_Args);
            ecf = new EventContentsForm(this, mode);
        }

        private void ArgsSet(int[] args)
        {
            if (args == null)
                return;

            numericUpDown1.Value = args[0];
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string[] code = { "LabelJump(" + numericUpDown1.Value + ")" };
            ecf.buttonOK_Close(code);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ecf.buttonCansel_Close();
        }

        private bool shift = false;
        private bool ctrl = false;
        private void timer1_Tick(object sender, EventArgs e)
        {
            // 誤動作防止
            if (Form.ActiveForm != this)
                return;

            shift = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftShift) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightShift);
            ctrl = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightCtrl);

            if (shift == true && ctrl == true)
                button1_Click(null, null);
        }
    }
}
