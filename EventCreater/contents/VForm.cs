﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class VForm : Form
    {
        int[] Args = new int[7];
        EventContentsForm ecf;
        ComponentsSync arsyn;
        public VForm(Form parent, int mode = 0, int[] e_Args = null)
        {
            // 個々のイベントコンテンツの共通処理
            InitializeComponent();

            ecf = new EventContentsForm(this, mode);
            ecf.parentForm = parent;

            Item.ItemLoadAndSet(comboBox1);
            Variable.CharaNameLoadAndSet(comboBox2);
            
            comboBox3.SelectedIndex = 0;
            comboBox4.SelectedIndex = 0;
            comboBox5.SelectedIndex = 0;
            comboBox6.SelectedIndex = 0;
            comboBox7.SelectedIndex = 0;

            // 各種オブジェクトをラジオボタンと連動させる。
            object[] radios = new object[20];
            object[] boxs = new object[20];

            radios[0] = radioButton1;
            radios[1] = radioButton1;
            radios[2] = radioButton2;
            radios[3] = radioButton2;
            radios[4] = radioButton3;
            radios[5] = radioButton3;
            radios[6] = radioButton10;
            radios[7] = radioButton11;
            radios[8] = radioButton11;
            radios[9] = radioButton12;
            radios[10] = radioButton12;
            radios[11] = radioButton13;
            radios[12] = radioButton13;
            radios[13] = radioButton14;
            radios[14] = radioButton14;
            radios[15] = radioButton15;
            radios[16] = radioButton15;
            radios[17] = radioButton16;
            radios[18] = radioButton16;
            radios[19] = radioButton17;

            boxs[0] = label1;
            boxs[1] = button1;
            boxs[2] = numericUpDown1;
            boxs[3] = numericUpDown2;
            boxs[4] = label2;
            boxs[5] = button2;
            boxs[6] = numericUpDown3;
            boxs[7] = label7;
            boxs[8] = button3;
            boxs[9] = label8;
            boxs[10] = button4;
            boxs[11] = numericUpDown4;
            boxs[12] = numericUpDown5;
            boxs[13] = comboBox1;
            boxs[14] = comboBox5;
            boxs[15] = comboBox2;
            boxs[16] = comboBox6;
            boxs[17] = comboBox3;
            boxs[18] = comboBox7;
            boxs[19] = comboBox4;

            arsyn = new ComponentsSync(radios, boxs);
            SetArgsToForm(e_Args);
        }
        
        private void SetArgsToForm(int[] ags)
        {
            if (ags == null) return;
            RadioButton[] rb1 = { radioButton1, radioButton2, radioButton3 };
            RadioButton[] rb2 = { radioButton4, radioButton5, radioButton6,
                                  radioButton7, radioButton8, radioButton9 };
            RadioButton[] rb3 = { radioButton10, radioButton11, radioButton12,
                                  radioButton13, radioButton14, radioButton15,
                                  radioButton16, radioButton17 };
            rb1[ags[0]].Checked = true;
            rb2[ags[3]].Checked = true;
            rb3[ags[4]].Checked = true;

            if (ags[0] == 0) label1.Text = Variable.GetVariableLabelText(ags[1]);
            else if (ags[0] == 2) label2.Text = Variable.GetVariableLabelText(ags[1]);
            else
            {
                numericUpDown1.Value = ags[1];
                numericUpDown2.Value = ags[2];
            }

            switch (ags[4])
            {
                case 0:
                    numericUpDown3.Value = ags[5];
                    break;
                case 1:
                    label7.Text = Variable.GetVariableLabelText(ags[5]);
                    break;
                case 2:
                    label8.Text = Variable.GetVariableLabelText(ags[5]);
                    break;
                case 3:
                    numericUpDown4.Value = ags[5];
                    numericUpDown5.Value = ags[6];
                    break;
                case 4:
                    comboBox1.SelectedIndex = ags[5];
                    comboBox5.SelectedIndex = ags[6];
                    break;
                case 5:
                    comboBox2.SelectedIndex = ags[5];
                    comboBox6.SelectedIndex = ags[6];
                    break;
                case 6:
                    comboBox3.SelectedIndex = ags[5];
                    comboBox7.SelectedIndex = ags[6];
                    break;
                case 7:
                    comboBox4.SelectedIndex = ags[5];
                    break;
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            ecf.buttonCansel_Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string[] code = new string[1];
            Args[0] = (radioButton1.Checked == true) ? 0 : Args[0];
            Args[0] = (radioButton2.Checked == true) ? 1 : Args[0];
            Args[0] = (radioButton3.Checked == true) ? 2 : Args[0];

            Args[1] = (radioButton1.Checked == true) ? Variable.GetVariableIndex(label1) : Args[1];
            Args[1] = (radioButton2.Checked == true) ? (int)numericUpDown1.Value : Args[1];
            Args[1] = (radioButton3.Checked == true) ? Variable.GetVariableIndex(label2) : Args[1];

            Args[2] = (radioButton2.Checked == true) ? (int)numericUpDown2.Value : 0;

            Args[3] = (radioButton4.Checked == true) ? 0 : Args[3];
            Args[3] = (radioButton5.Checked == true) ? 1 : Args[3];
            Args[3] = (radioButton6.Checked == true) ? 2 : Args[3];
            Args[3] = (radioButton7.Checked == true) ? 3 : Args[3];
            Args[3] = (radioButton8.Checked == true) ? 4 : Args[3];
            Args[3] = (radioButton9.Checked == true) ? 5 : Args[3];

            Args[4] = (radioButton10.Checked == true) ? 0 : Args[4];
            Args[4] = (radioButton11.Checked == true) ? 1 : Args[4];
            Args[4] = (radioButton12.Checked == true) ? 2 : Args[4];
            Args[4] = (radioButton13.Checked == true) ? 3 : Args[4];
            Args[4] = (radioButton14.Checked == true) ? 4 : Args[4];
            Args[4] = (radioButton15.Checked == true) ? 5 : Args[4];
            Args[4] = (radioButton16.Checked == true) ? 6 : Args[4];
            Args[4] = (radioButton17.Checked == true) ? 7 : Args[4];

            Args[5] = (radioButton10.Checked == true) ? (int)numericUpDown3.Value : Args[5];
            Args[5] = (radioButton11.Checked == true) ? Variable.GetVariableIndex(label7) : Args[5];
            Args[5] = (radioButton12.Checked == true) ? Variable.GetVariableIndex(label8) : Args[5];
            Args[5] = (radioButton13.Checked == true) ? (int)numericUpDown4.Value : Args[5];
            Args[5] = (radioButton14.Checked == true) ? comboBox1.SelectedIndex : Args[5];
            Args[5] = (radioButton15.Checked == true) ? comboBox2.SelectedIndex : Args[5];
            Args[5] = (radioButton16.Checked == true) ? comboBox3.SelectedIndex : Args[5];
            Args[5] = (radioButton17.Checked == true) ? comboBox4.SelectedIndex : Args[5];

            Args[6] = (radioButton10.Checked == true) ? 0 : Args[6];
            Args[6] = (radioButton11.Checked == true) ? 0 : Args[6];
            Args[6] = (radioButton12.Checked == true) ? 0 : Args[6];
            Args[6] = (radioButton13.Checked == true) ? (int)numericUpDown5.Value : Args[6];
            Args[6] = (radioButton14.Checked == true) ? comboBox5.SelectedIndex : Args[6];
            Args[6] = (radioButton15.Checked == true) ? comboBox6.SelectedIndex : Args[6];
            Args[6] = (radioButton16.Checked == true) ? comboBox7.SelectedIndex : Args[6];
            Args[6] = (radioButton17.Checked == true) ? 0 : Args[6];

            code[0] = "Variable(" + Args[0] + "," + Args[1] + "," + Args[2] + "," + Args[3] + "," + Args[4] + "," + Args[5] + "," + Args[6] + ")";

            ecf.buttonOK_Close(code);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(1, (int a, string vnamecomp) =>
            {
                label1.Text = vnamecomp;
            });
            CloseAct.FormOpenCommon(this, vf);
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(1, (int a, string vnamecomp) =>
            {
                label2.Text = vnamecomp;
            });
            CloseAct.FormOpenCommon(this, vf);
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(1, (int a, string vnamecomp) =>
            {
                label7.Text = vnamecomp;
            });
            CloseAct.FormOpenCommon(this, vf);
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(1, (int a, string vnamecomp) =>
            {
                label8.Text = vnamecomp;
            });
            CloseAct.FormOpenCommon(this, vf);
        }

        private bool shift = false;
        private bool ctrl = false;
        private void timer1_Tick(object sender, EventArgs e)
        {
            // 誤動作防止
            if (Form.ActiveForm != this)
                return;

            shift = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftShift) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightShift);
            ctrl = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightCtrl);

            if (shift == true && ctrl == true)
                button2_Click(null, null);
        }
    }
}