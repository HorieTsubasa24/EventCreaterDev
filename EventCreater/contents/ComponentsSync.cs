﻿using System;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventCreater
{
    /// <summary>
    /// フォームに選択中のボタン、チェックボックス、ラジオボタンを見て
    /// 選択中のcomboBox, textBoxフォームの有効化をする。
    /// </summary>
    public class ComponentsSync
    {
        /// <summary>
        /// ラジオボタンなどのコンポーネント
        /// </summary>
        public List<object> choiceparts;
        /// <summary>
        /// 有効化させるコンポーネント(添字をchoicepartsと合わせる。)
        /// </summary>
        public List<object> enableparts;

        /// <summary>
        /// ラジオボタンとボックスを同期する。
        /// </summary>
        /// <param name="radios"></param>
        /// <param name="boxs"></param>
        public ComponentsSync(object[] radios, object[] boxs)
        {
            choiceparts = new List<object>();
            enableparts = new List<object>();

            for (int i = 0; i < radios.Length; i++)
            {
                choiceparts.Add(radios[i]);
                enableparts.Add(boxs[i]);
                var a = (RadioButton)choiceparts[i];
                a.Click += new EventHandler(OnClick);
            }
        }

        /// <summary>
        /// ラジオボタンとボックスを[グループ, 個数]の順でクラス外から自分でセット。
        /// </summary>
        /// <param name="radios"></param>
        /// <param name="boxs"></param>
        public ComponentsSync()
        {
            choiceparts = new List<object>();
            enableparts = new List<object>();
        }
        
        /// <summary>
        /// ラジオボタンクリック時にボックスの有効化、無効化を切り替える。
        /// </summary>
        private void OnClick(object sender, EventArgs e)
        {
            async();
        }

        public void async()
        {
            for (int i = 0; i < choiceparts.Count; i++)
            {
                var a = (RadioButton)choiceparts[i];
                Control b = null;
                if (enableparts[i] != null)
                {
                    b = (Control)enableparts[i];
                    b.Enabled = a.Checked;
                }
            }
        }
    }
}
