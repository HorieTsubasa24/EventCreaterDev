﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class ChoiceForm : Form
    {
        EventContentsForm ecf;
        public ChoiceForm(Form parent, int mode = 0, string e_St = null, int[] e_Args = null)
        {
            // 個々のイベントコンテンツの共通処理
            InitializeComponent();
            SetArgsToForm(e_St, e_Args);

            ecf = new EventContentsForm(this, mode);
        }

        private void SetArgsToForm(string str, int[] ags)
        {
            if (ags == null) return;
            string[] words = str.Split('/');
            TextBox[] tbs = { textBox1, textBox2, textBox3, textBox4 };

            for (int i = 0; i < tbs.Length; i++)
                tbs[i].Text = "";

            for(int i = 0; i < words.Length; i++)
                tbs[i].Text = words[i];

            RadioButton[] rb1 = { radioButton1, radioButton2, radioButton3,
                                  radioButton4, radioButton5, radioButton6 };
            rb1[ags[0]].Checked = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int rad = 0;
            rad = (radioButton1.Checked == true) ? 0 : rad;
            rad = (radioButton2.Checked == true) ? 1 : rad;
            rad = (radioButton3.Checked == true) ? 2 : rad;
            rad = (radioButton4.Checked == true) ? 3 : rad;
            rad = (radioButton5.Checked == true) ? 4 : rad;
            rad = (radioButton6.Checked == true) ? 5 : rad;

            string[] texts = new string[4];
            texts[0] = textBox1.Text;
            texts[1] = textBox2.Text;
            texts[2] = textBox3.Text;
            texts[3] = textBox4.Text;
            string strs = "";
            int num = 1;

            for(int i = texts.Length - 1; i >= 0; i--)
                if(texts[i] != "")
                {
                    num = i + 1;
                    break;
                }

            for(int i = 0; i < num; i++)
            {
                strs += texts[i] + "/";
            }
            // 端の/は削除
            strs = strs.Remove(strs.Length - 1, 1);

            int cancelb = (rad == 5) ? 2 : 0;
            string[] code = new string[2 + num * 2 + cancelb];

            code[0] = "Choice(\"" + strs + "\"," + rad + ")";
            code[code.Length - 1] = "EndChoice:";
            for(int i = 0; i < num; i++)
            {
                code[1 + i * 2] = "Branch(\"" + texts[i] + "\"," + i + ")";
                code[1 + i * 2 + 1] = "";
            }
            if (rad == 5)
            {
                code[code.Length - 3] = "Branch(4)";
                code[code.Length - 2] = "";
            }
            ecf.buttonOK_Close(code);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        private string befstrbkup = "";
        // 文字"/"の入力禁止
        private void textbox_TextChanged(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text.IndexOf('/') != -1 || tb.Text.IndexOf('\"') != -1)
            {
                MessageBox.Show("'/', '\"'は入力できません。");
                tb.Text = befstrbkup;
            }
        }

        private void textBox_Enter(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            befstrbkup = tb.Text;
        }

        private bool shift = false;
        private bool ctrl = false;
        private void timer1_Tick(object sender, EventArgs e)
        {
            // 誤動作防止
            if (Form.ActiveForm != this)
                return;

            shift = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftShift) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightShift);
            ctrl = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightCtrl);

            if (shift == true && ctrl == true)
                button1_Click(null, null);
        }
    }
}
