﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class FlashForm : Form
    {
        int[] Args = new int[6];
        EventContentsForm ecf;
        public FlashForm(Form parent, int mode = 0, int[] e_Args = null)
        {
            // 個々のイベントコンテンツの共通処理
            InitializeComponent();

            ecf = new EventContentsForm(this, mode);
            SetArgsToForm(e_Args);
        }

        private void SetArgsToForm(int[] ags)
        {
            if (ags == null) return;
            numericUpDown1.Value = ags[0];
            numericUpDown2.Value = ags[1];
            numericUpDown3.Value = ags[2];
            numericUpDown4.Value = ags[3];
            numericUpDown5.Value = ags[4];
            checkBox1.Checked = Convert.ToBoolean(ags[5]);
            NumToBar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Args[0] = (int)numericUpDown1.Value;
            Args[1] = (int)numericUpDown2.Value;
            Args[2] = (int)numericUpDown3.Value;
            Args[3] = (int)numericUpDown4.Value;
            Args[4] = (int)numericUpDown5.Value;
            Args[5] = Convert.ToInt32(checkBox1.Checked);

            string[] code = new string[1];
            code[0] = "ScreenFlash(" + Args[0] + "," + Args[1] + "," + Args[2]  + "," + Args[3] + "," + Args[4] + "," + Args[5] + ")";
            ecf.buttonOK_Close(code);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ecf.buttonCansel_Close();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            BarToNum();
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            BarToNum();
        }

        private void trackBar3_Scroll(object sender, EventArgs e)
        {
            BarToNum();
        }

        private void trackBar4_Scroll(object sender, EventArgs e)
        {
            BarToNum();
        }

        private void BarToNum()
        {
            numericUpDown1.Value = trackBar1.Value;
            numericUpDown2.Value = trackBar2.Value;
            numericUpDown3.Value = trackBar3.Value;
            numericUpDown4.Value = trackBar4.Value;
            LabelColCng();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            NumToBar();
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            NumToBar();
        }

        private void numericUpDown3_ValueChanged(object sender, EventArgs e)
        {
            NumToBar();
        }

        private void numericUpDown4_ValueChanged(object sender, EventArgs e)
        {
            NumToBar();
        }

        private void NumToBar()
        {
            trackBar1.Value = (int)numericUpDown1.Value;
            trackBar2.Value = (int)numericUpDown2.Value;
            trackBar3.Value = (int)numericUpDown3.Value;
            trackBar4.Value = (int)numericUpDown4.Value;
            LabelColCng();
        }

        // numからlabelの色変更
        private void LabelColCng()
        {
            byte R = (byte)((numericUpDown1.Value + 1) * 8 - 1);
            byte G = (byte)((numericUpDown2.Value + 1) * 8 - 1);
            byte B = (byte)((numericUpDown3.Value + 1) * 8 - 1);
            byte A = (byte)((numericUpDown4.Value + 1) * 8 - 1);
            Color col = Color.FromArgb(A, R, G, B);
            label1.BackColor = col;
        }

        private bool shift = false;
        private bool ctrl = false;
        private void timer1_Tick(object sender, EventArgs e)
        {
            // 誤動作防止
            if (Form.ActiveForm != this)
                return;

            shift = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftShift) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightShift);
            ctrl = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightCtrl);

            if (shift == true && ctrl == true)
                button1_Click(null, null);
        }
    }
}
